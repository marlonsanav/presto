<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'presto' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}m*ss9o.4SJ{nsh4C.Rx/,O sB  8}#5++x@_.6} u&(FBxy,]n#g.d#[-Q`gRvC' );
define( 'SECURE_AUTH_KEY',  'B`=4)p$0)HWOI3vFfNaLDR`UhG,N<]TG}Y/gRRo2cm]K&~RC#5}fZ+tMq*2fyWu1' );
define( 'LOGGED_IN_KEY',    'qi`C>cxPEviR-x$tA~%F<p&k&B8tZo$(Ih4M|gfH/pRh,R;?8+&x)NHTY:v3|9E$' );
define( 'NONCE_KEY',        '!~H![*pu? V|I0N3^y7V1Y(3|,2+,C:,dU]IMF}?<-z{Kon,Ff%^o1dTPR<cewxl' );
define( 'AUTH_SALT',        'loM.PW8yB[A)CS@<uN781|9Z<T2gcfa.kk1yK#Q Xiay{RJy|.0rAy*r__BeA16+' );
define( 'SECURE_AUTH_SALT', '$mjklUqm[pIu(*VM_$c}<)ZynLEdX6ctRD16rRoY-EB&wI9uM^Bv]$/)l?uS3rii' );
define( 'LOGGED_IN_SALT',   'e^ttZr1cetF_,wHsX.9/.Vv~._lGy4+@S=I /)jh 34w}>m:Kk@Fnv@jJc[eZ3&Q' );
define( 'NONCE_SALT',       '{U8uh=I`kn9c/%$9]Fkg03/ERsO^H3I41Gx~?N@:CB_ aAs|>S7qL1}o;/S1:<p:' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
